import FeatureExtraction
import NoiseReduction
import CrossCorrelation
import FormulateConstraints
import ConstraintSatisfaction
from scipy import io
from datetime import datetime
import numpy as np
import glob
import matplotlib.pyplot as plt
import utils


def main():
    for file in [x for x in glob.glob("recordings/*.wav") if x != "recordings\\Noise.wav"]:
        rate, data = io.wavfile.read(file)
        _, noise = io.wavfile.read("recordings/noise.wav")
        file = file.split("\\")[1]
        print(file)
        noise_reduced = NoiseReduction.reduce_noise(rate, data, 10, noise=noise)
        keystroke_indexes = FeatureExtraction.feature_extraction(noise_reduced, rate, file, True)
        similarity = CrossCorrelation.cross_correlate(noise_reduced, keystroke_indexes, True)
        constraints = FormulateConstraints.formulate_constraints(similarity, True)
        words = ConstraintSatisfaction.find_words(constraints, len(keystroke_indexes))
        print("Top words:", words[:100], len(words))
        with open("KeystrokeIndexes.txt") as f:
            for x in f.readlines():
                if x.split('=')[0] == file:
                    actual_word = x.split('=')[1].split(':')[0]
                    break
        for index, x in enumerate(words):
            if x[0] == actual_word:
                print("Actual word at index "+str(index))
                break


if __name__ == '__main__':
    main()
