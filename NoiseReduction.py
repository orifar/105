import noisereduce as nr
from scipy import io
import numpy


def reduce_noise(rate, data, noise_part_seconds, output_file_name="reduced_noise.wav",
                 verbose=False, noise=None, return_output=True):
    """Take a wav file that contains background noise and remove it.

    :param rate: The rate of the wav file that will be denoised
    :param data: The data of the wav file that will be denoised
    :param noise_part_seconds: The amount of seconds in the beginning of the sound file that represent the noise to be
    removed. If noise is in seperate file then this should be 0.
    :param output_file_name: The name of the file that the denoised data will be saved in. (default reduced_noise.wav)
    :param verbose: Verbose argument for noisereduce library. True will show graphs of original data,
    noise and denoised data. (default False)
    :param noise: Optional noise data if noise is in separate file. (default None)
    :param return_output: If True the denoised file will be returned instead of being saved to file. (default True)
    :returns: None if return_output=False and numpy float16 array if return_output=True
    """

    # convert from int array to float array
    data = data.astype(numpy.float16, order='C')

    # take first ten seconds as noise sample
    if noise is None:
        noise = data[:rate*noise_part_seconds]
        data = data[rate*noise_part_seconds:]
    else:
        noise = noise.astype(numpy.float16, order='C')

    # perform noise reduction
    reduced_noise = nr.reduce_noise(audio_clip=data, noise_clip=noise, verbose=verbose)

    if return_output:
        return reduced_noise
    else:
        # output the resulting wav files
        io.wavfile.write(output_file_name, rate, reduced_noise.astype(numpy.int16, order='C'))
