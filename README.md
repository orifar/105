# Horton
Horton is a program that aims to identify a word typed on a mechanical keyboard based on the acoustic emanations created from that keyboard. The tool will allow users to know what word was typed on keyboard while accessing only a microphone located nearby. Horton can enable users to type on keyboard without connecting it to the computer.

## Existing Implementations

 - [Dictionary Attacks Using Keyboard Acoustic Emanations](https://www.eng.tau.ac.il/~yash/p245-berger.pdf)
 - [Keyboard Acoustic Emanations](https://rakesh.agrawal-family.com/papers/ssp04kba.pdf)
 - [Keyboard Acoustic Emanations Revisited](https://people.eecs.berkeley.edu/~tygar/papers/Keyboard_Acoustic_Emanations_Revisited/preprint.pdf)
 - [(sp)iPhone: Decoding Vibrations From Nearby Keyboards Using Mobile Phone Accelerometers](https://cse.sc.edu/~wyxu/719Spring12/papers/US-vibr-Phone.pdf)

 The last one focuses on data from accelerometers, but still employs certain techniques we might find useful.

## Current Progress
Until now we have mainly focused on finding the existing implementations and understanding how we want to implement the idea ourselves.

We have decided on most of the high-level details but we still need to choose a programming language suitable for signal processing (probably Python).
