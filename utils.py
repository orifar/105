import numpy as np
import enum


class Constraint(enum.Enum):
    NONE = 0
    EQ = 1
    ADJ = 2
    NEAR = 3
    DIST = 4


def normalize(arr, low, high):
    if max(arr) == min(arr):
        return np.zeros(len(arr))
    return low + ((arr-min(arr))*(high-low))/(max(arr)-min(arr))
