from scipy import io
import numpy as np
import itertools as it
import matplotlib.pyplot as plt
import sys
from scipy import signal
import utils


def cross_correlate(audio, keystroke_indexes, visualize):
    """get the maximum cross correlation between each two push peaks
    :param audio: numpy array of audio from typing recording
    :param keystroke_indexes: indexes of each keystroke
    :param visualize: if this value is True, the function will display a similarities table
    """

    # normalizing the audio and creating empty arrays for the css results
    audio = utils.normalize(audio, -1, 1)
    push_ccs = np.zeros([len(keystroke_indexes), len(keystroke_indexes)])
    release_ccs = np.zeros([len(keystroke_indexes), len(keystroke_indexes)])

    # going over each pair of keystrokes
    for index1, keystroke1 in enumerate(keystroke_indexes):
        for index2, keystroke2 in enumerate(keystroke_indexes):

            # extracting the push and release phases
            push1, push2 = audio[keystroke1[0][0]:keystroke1[0][1]], audio[keystroke2[0][0]:keystroke2[0][1]]
            release1, release2 = audio[keystroke1[1][0]:keystroke1[1][1]], audio[keystroke2[1][0]:keystroke2[1][1]]

            # applying the comparison function (cross correlation)
            push_ccs[index1][index2] = max(np.correlate(push1, push2))
            push_ccs[index1][index2] = abs(push_ccs[index1][index1] - push_ccs[index1][index2])
            release_ccs[index1][index2] = max(np.correlate(release1, release2))
            release_ccs[index1][index2] = abs(release_ccs[index1][index1] - release_ccs[index1][index2])

            # calculating the final similarities by the mean scheme
            final_ccs = (push_ccs+release_ccs)/2

    # calculating final similarities by flipping the final cross correlation
    for index, cc in enumerate(final_ccs):
        final_ccs[index] = 1-utils.normalize(cc, 0, 1)

    # drawing the graph
    if visualize:
        plt.imshow(final_ccs, cmap='hot')
        plt.show()

    return final_ccs
