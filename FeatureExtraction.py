import matplotlib.pyplot as plt
from scipy import fftpack
from scipy.io import wavfile # get the api
import scipy
import numpy as np
import math
import utils
import exceptions
import NoiseReduction
import CrossCorrelation as cc
# the thresholds corresponding to a click
PUSH_THRESHOLD = 0.118
PUSH_FALL_THRESHOLD = -0.068
RELEASE_THRESHOLD = 0.102
RELEASE_FALL_THRESHOLD = -0.070
PUSH_RELEASE_MIN_DIST = 7
KEYSTROKE_MIN_DIST = 8
CLICK_LENGTH = 300  # the estimated click length in ms
WINDOW_PER_SEC = 120


def extract_keypress_bins(delta_vector):
    """
    This function returns the energy bins corresponding to clicks
    :param delta_vector: the difference between every two consecutive fft energy bins
    :returns: indexes for the bins corresponding click in this format:
    ((push_peak, end_of_push_peak), (release_peak, end_of_release_peak))
    """
    clicks = []
    bins_per_click = (CLICK_LENGTH / 1000) * WINDOW_PER_SEC

    # iterating in a while loop, so we can skip some of the elements
    counter = 0
    while counter < len(delta_vector):
        energy_bin = delta_vector[counter]
        # if the difference in energy is enough to account for a push peak
        if energy_bin >= PUSH_THRESHOLD:
            push_loc = counter
            current_click = delta_vector[push_loc:push_loc + int(bins_per_click)]
            try:
                # looking for a fall, then a raise in energy, and then another fall.
                # if one of those is not found in the duration of the click, we ignore the current push energy_bin.
                push_fall_loc = find_next(current_click, PUSH_FALL_THRESHOLD, True)
                release_loc = find_next(current_click[push_fall_loc + PUSH_RELEASE_MIN_DIST:], RELEASE_THRESHOLD) + PUSH_RELEASE_MIN_DIST
                release_fall_loc = find_next(current_click[release_loc:], RELEASE_FALL_THRESHOLD, True)
            except exceptions.ValueNotFoundError:
                counter += 1
                continue
            # skipping the bins contained in the click
            counter += release_fall_loc + release_loc + push_fall_loc
            # making the relative offsets absolute addresses
            push_fall_loc += push_loc
            release_loc += push_fall_loc
            release_fall_loc += release_loc
            try:
                # setting the start of the click to the first location with a small value
                push_loc -= find_next(delta_vector[push_loc::-1], RELEASE_THRESHOLD, True)
            except exceptions.ValueNotFoundError:
                pass
            # add 2 bins to the end of each segment to include end of press and release
            clicks.append(((push_loc, push_fall_loc+2), (release_loc, release_fall_loc+5)))
            counter = release_fall_loc + KEYSTROKE_MIN_DIST
        counter += 1
    return clicks


def find_next(vector, value, is_smaller=False):
    """
    This function searches a list for a value larger or smaller then a given value
    :param vector: the list to search
    :param value: the threshold value we are looking for
    :param is_smaller: weather we should look for a value smaller or larger then the threshold
    :returns: in success, the function returns the first element matching the threshold
    :raises: ValueNotFoundError
    """
    for count, energy_bin in enumerate(vector):
        if is_smaller:
            if energy_bin < value:
                return count
        else:
            if max(vector[count:count+5]) == energy_bin > value:
                return count
    raise exceptions.ValueNotFoundError


def calculate_bins(normalized, audio_len, rate):
    """ This function calculates the energy bins of a signal.
    The fft window size is defined iFn a global variable
    :param normalized: the normalized audio signal
    :param audio_len: the length if the audio signal
    :param rate: the signal's sampling rate
    :returns: the energy bins corresponding to the audio
    """
    bins = []
    for seg in np.array_split(normalized, (audio_len/rate) * WINDOW_PER_SEC):
        # applying the window function and calculating the FFT coefficients
        # of the entire segment ('window') to get the energy bins
        win = np.kaiser(len(seg), 5)
        fft_audio = fftpack.fft(seg*win)  # calculate fourier transform (complex numbers list)
        fft_len = len(fft_audio)//2  # you only need half of the fft list (real signal symmetry)
        bins.append(np.sum(np.abs(fft_audio[:fft_len-1])))

    # normalizing the bins and calculating the delta vector
    bins = list(utils.normalize(np.asarray(bins, dtype=np.float), 0, 1))
    return bins


def visualize(audio, rate, keystroke_milliseconds, dots=False, sections=True):
    """
    This function plots the signal's graph and print's the keystrokes
    :param audio: the raw signal
    :param rate: the sampling rate of the signal
    :param delta_vector: the difference between every two subsequent fft energy bins
    :param keystroke_bins: the indexes of the keystrokes in the energy bins
    :param keystroke_milliseconds: the indexes of the keystrokes in the original wave
    :param dots: if true the peaks will be drawn
    :param sections: if true each keystroke will be highlighted
    """
    plt.subplot(1, 1, 1)
    # plotting with time in seconds on the x axis
    times = [1000*i/rate for i in range(0, len(audio))]
    plt.plot(times, audio)
    if dots:
        push_start = [x[0][0] for x in keystroke_milliseconds]
        release_start = [x[1][0] for x in keystroke_milliseconds]
        plt.plot(push_start, [0 for i in push_start], "o")
        plt.plot(release_start, [0 for i in release_start], "o")
    if sections:
        for click in keystroke_milliseconds:
            plt.axvspan(click[0][0], click[1][1], alpha=0.5)


    plt.show()


def feature_extraction(audio, rate, file_name, graph):
    with open("KeystrokeIndexes.txt") as f:
        content = f.readlines()
        for x in content:
            if x.split("=")[0] == file_name:
                indexes = eval(x.split("=")[1].split(":")[-1])
                if graph:
                    visualize(audio, rate, indexes, True)
                return indexes


def main():
    rate, audio = NoiseReduction.reduce_noise("DifferenceNew.wav", 10, return_output=True)
    cc.max_cross_correlate(utils.normalize(audio, -1, 1), feature_extraction(rate, audio, False))


if __name__ == '__main__':
    main()
