from utils import Constraint
import numpy as np
import random

NUM_OF_CYCLES = 2000


def find_words(constraints, word_len):
    """
    finds all words in dictionary that fit in with the constraints that were found
    :param constraints: a list of all constraints found
    :param word_len: the length of the word
    :return: a list of possible words with the number of constraint cycles that each one passed
    """
    big_dict = extract_keyboard_dict()
    possible_words = {}
    constraints = list({tuple(sorted(x[0])):x[1] for x in constraints}.items())
    for x in range(NUM_OF_CYCLES):
        constraint_sample = get_sample(constraints, 0.3)
        with open("wordlist/words"+str(word_len)+".txt") as wordlist:
            for word in wordlist.read().split("\n")[:-1]:
                for constraint in constraint_sample:
                    if not word[constraint[0][0]] in big_dict[word[constraints[0][1]]][constraint[1]]:
                        break
                else:
                    if word in possible_words:
                        possible_words[word] += 1
                    else:
                        possible_words[word] = 1
    return sorted(possible_words.items(), key=lambda x: x[1], reverse=True)


def extract_keyboard_dict():
    """
    get the dictionary representing the distance between every two keys on the keyboard
    :return: the dictionary represented in keyboard.txt
    """
    with open("keyboard.txt") as keyboard:
        data = keyboard.readlines()
        data = [x.split(" ") for x in data]
        big_dict = {}
        for key in data:
            key = [x.split(",") for x in key]
            big_dict[key[0][0]] = {Constraint.EQ.value: key[0], Constraint.ADJ.value: key[1], Constraint.NEAR.value: key[2], Constraint.DIST.value: key[3]}
        return big_dict


def get_sample(constraints, probability):
    """
    get a sample from the array constraints where each value has a certain probability to appear
    :param constraints: the list of constraints
    :param probability: the probability of each value appearing in the sample
    :return: a sample of constraints
    """
    s = []
    for x in constraints:
        if random.randint(0, 10) <= probability*10:
            s.append(x)
    return s
