class Error(Exception):
    """Base class for our exceptions"""
    pass
class ValueNotFoundError(Error):
    """Raised when a value was not found in a list"""
    pass
