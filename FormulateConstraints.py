import enum
import numpy as np
import itertools
import matplotlib.pyplot as plt
from utils import Constraint
import matplotlib.patches as mpatches


def formulate_constraints(matrix, visualize):
    """
    This function returns a matrix of constraints given the similarity matrix.
    It uses the BestFriendsPick policy described in section 4.1 of Dictionary Attacks Using Keyboard Acoustic Emanations.
    The function returns a matrix with values in the Constraint enum.
    :param matrix: the similarity matrix
    :param visualize: if this value is True, a table will be displayed
    """

    mat_len = len(matrix)
    # an array for final constraints, and an array for ranks
    constraints = np.zeros(shape=(mat_len, mat_len), dtype=int)
    ranks = get_ranks(matrix)

    # going over every pair only once
    for row, col in itertools.combinations(range(mat_len), 2):
        #calculating the constraint based on the ranks calculated above
        constraint = get_constraint(ranks[row][col], ranks[col][row], mat_len)
        constraints[row][col] = constraint.value
        constraints[col][row] = constraint.value

    if visualize:
        plt.imshow(constraints)
        plt.show()

    # changing the new constraints format to a touple:
    # ((Key1, Key2), Constraint)
    return [x for x in np.ndenumerate(constraints) if x[1] != Constraint.NONE.value]


def get_constraint(rank1, rank2, keys_len):
    """
    This function returns a constraint from the above enum given the matching ranks of 2 keys.
    Given two keys, i and j:
    :param rank1: rank(j, i)
    :param rank2: rank(i, j)

    rank(i, j) is defined in section 4.1 of Dictionary Attacks Using Keyboard Acoustic Emanations.
    This function is an implementation of Table 2 in the same article.
    """
    sum = rank1 + rank2

    # see Table 2
    if sum <= 3:
        return Constraint.EQ
    if sum == 4:
        return Constraint.ADJ
    if sum == 5:
        return Constraint.NEAR
    if sum >= 2*keys_len - 1:
        return Constraint.DIST

    return Constraint.NONE


def get_ranks(matrix):
    """
    This function calculates the rank matrix.
    it sorts each row if the similarity matrix to achive this
    :param matrix: the similarity matrix

    it returns a 2d numpy array where rank(i,j) = ranks[i][j]
    rank(i, i) is defined as 0.
    """

    ranks = np.zeros(shape=(len(matrix), len(matrix)))

    # going over each row and getting all of it's ranks.
    # for example, in the first iteration it calculates rank(1, j)
    for row_index, row in enumerate(matrix):

        # making an indexed row so that each cell looks like (similarity_value, column)
        indexed_row = list()
        for col_index, value in enumerate(row):
            indexed_row.append((value, col_index))

        # sorting the row by the similarity value
        indexed_row.sort(key= lambda tup:tup[0], reverse=True)
        # going over the sorted row. the first elemnt has rank 1, the second has rank 2, and so on
        for rank, cell in  enumerate(indexed_row):
            ranks[row_index][cell[1]] = rank

    return ranks
